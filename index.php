<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	
	if (isset($_POST['startDate'])) { // Check if the user send the form
	$startDate = $_POST['startDate'];
	$startTime = $_POST['startTime'];
	$startTime2 ="00:00:00.000Z";
	$endDate = $_POST['endDate']; 
	$endTime = $_POST['endTime'];
	$difference = $_POST['difference'];
	$location = $_POST['location'];
	
	if ($location=="Haifa"){
		$location = "Haifa";
	}
	else{
		$location = "Jerusalem";
	}


  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'Elad is my king',
  'location' => $location,
  'description' => 'elad is my king',
  'start' => array(
    'dateTime' => $startDate.'T'.$startTime.':00+02:00',// 9 hours before, 2 hours different
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => $endDate.'T'.$endTime.':00+02:00', //9 hours before, 3 hours different
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=1'
  ),
  'attendees' => array(
    array('email' => 'alad17@gmail.com'),
    array('email' => 'tomwininger1@gmail.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
 
	}// end if isset
	else{ //start else isset
		$client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'tom is gay',
  'location' => 'Jerusalem',
  'description' => 'Tom is gay',
  'start' => array(
    'dateTime' => '2017-02-28T05:00:00-03:00',// 9 hours before, 2 hours different
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => '2017-02-28T06:00:00-03:00', //9 hours before, 3 hours different
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=1'
  ),
  'attendees' => array(
    array('email' => 'alad17@gmail.com'),
    array('email' => 'tomwininger1@gmail.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
	} // end else isset
} 
	else { // start else for big if
  $redirect_uri =  'http://eladco.myweb.jce.ac.il/calendar/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

}